## Angular Ivy and bundle sizes
#### Can we build js13k games with Angular yet?

Note: The new rendering engine for Angular, Ivy, is shipped. You can use it in your apps.  One of the more interesting promises of the new rendering engine is that the bundle sizes of the finished applications could get a lot smaller with Ivy. But why does it even matter? For this talk I'm going to explore this topic a bit more. Is this already usable? How can we try it? How big is the regular angular bundle compared to the Ivy bundle? Did the new differential loader help, and how much? Are there more things coming in the future that will help us with the bundle size?

---

### js13k

> Js13kGames is a JavaScript coding competition for HTML5 Game Developers. The fun part of the compo is the file size limit set to 13 kilobytes.
>
> -- <cite>[js13kGames][1]</cite>

[1]:https://js13kgames.com


----

### The main plot point

Can we pack an Angular app into 13k zipped?

----

### Example games: Underrun

Underrun 

<a target="blank" href="https://js13kgames.com/entries/underrun"><img src="img/underrun.png"></a>


----

### Example games: Space Ducts

Space Ducts

<a target="blank" href="https://js13kgames.com/games/spaceducts/index.html"><img src="img/spaceducts.png"></a>

----

### Dam! VR Games in 13k?

Lasergrid <span class="small-print">Small print</span>

<a target="blank" href="https://js13kgames.com/entries/lasergrid"><img src="img/lasergrid.jpg"></a>



<span class="small-print">Small print: Framework itself (a-frame, three.js etc) is forgiven</span>

----

### Surely we can have Angular hello world there as well!


---

### Creating the smallest Angular app

We have Angular 8.3, that's going to do nicely, stable Node.

----

#### Step 1: generate a new app

<img src="img/generate.png">

----

#### Step 2: build the app

<img src="img/build.png">

----

#### Step 3: Pack it all into a nice lil' zip

<img src="img/zip.png">

----

# WHAT???

<img src="img/zip-zoom.png">

---

### Wait, let's optimize it all

- cut out styles 
- inline the template
- nothing else, just this text!

<img src="img/optimize2.png" width="300"> 
<img src="img/optimize.png" height="200">


----

### Build the thing

Let's add optimizations:

> The --prod meta-flag engages the following build optimization features.
> <span class="right"><cite>[Angular.io](https://ngular.io)</cite></span>


----

### What does it do?

Prod build:

- Ahead-of-Time (AOT) Compilation: pre-compiles Angular component templates.
- Production mode: deploys the production environment which enables production mode.
- Bundling: concatenates your many application and library files into a few bundles.
- Minification: removes excess whitespace, comments, and optional tokens.
- Uglification: rewrites code to use short, cryptic variable and function names.
- Dead code elimination: removes unreferenced modules and much unused code.

----

### Rebuild and zip

<img src="img/build-optim.png">

<img src="img/zip-optim.png">


----

### Result?

<img src="img/optim-result.png">

Much better!

----

### What if it was gzipped?

- gzip has better compression

<img src="img/optim-gzipped.png">


---

### Lessons learned

- Don't ship your production builds without `-prod` flag!


---

### Further with the example

Missing features:


- It's uglier then the games
- It scores super bad on Lighthouse
- (Obviously) 130k-140k > 13k

----

#### Last but not least

- It doesn't do anything.


----

## THE OTHER GAME RUNS VR!


---

### What's in the bundle


      "scripts": {
        "ng": "ng",
        "start": "ng serve",
        "build": "ng build --prod",
        "build:stats": "ng build --prod --stats-json",
        "analyze": "webpack-bundle-analyzer dist/demo-pre-ivy/stats-es2015.json",
        ...
      },


----

### Zlatko, now show them analyzer.
 

----

### Well, did ya?

Just run `npm run analyze:pre-ivy` you doofus! 

---




### Ivy

The Angular team saw the same problem with this.

----

They really wanted to win some js13k prizes!


----

Decided to rebuild the angular compiler and build pipeline.

---

The new thing is called Ivy.

---

### About Ivy

- It's (mostly) internal changes. 
- Your old code should be (almost) fully compatible.
- _Preview_ version is shipped with Angular 8.
- You can enable it, and _most_ apps should work*.
- Angular team recommends that you do enable it, run it, find bugs and document them.
- But do not push to production until Angular 9.

<span class="left small-print">Oh, wait for the surprise.</span>

---

### Let's try with the new Ivy compiler


---

### Generate a new app

<img src="img/ivi-gen.png">


----

### Refactor all the <strike>crap</strike> <strike>boilerplate</strike> default content

- let's aim for the smallest result

<img src="img/ivy-refactor.png">


----

### Build --prod right away

<img src="img/ivy-build.png"> 

----

### CSS is actually a Stylesheet file!

<img src="img/ivy-build-css.png"> 


----

### Zip

<img src="img/ivy-zip.png">


----

### Aaaand...


----

### This slide intentionally left blank

----

<img src="img/ivy-result.png">


----

### Compare it to previous:


Old:

<img src="img/optim-result.png">

New:

<img src="img/ivy-result.png">


----

### &lt;disapointed emoji/&gt;


----

### Gzipped is not better :(

<img src="img/ivy-gzipped.png">


---

### Real life example


- Upgrading an Angular 7 Application

<img src="img/build-no-ivy.png">

----

### Angular 8 is bigger then 7?

Angular 7

<img src="img/gzip-no-ivy.png">

Angular 8

<img src="img/gzip-no-ivy-8.png">


----

### What?

----

### It's about differential loading!

<img src="img/es5-es2015.png"> 

----

### Angular 8, but only for modern browsers

<img src="img/gzip-no-ivy-8-es2015.png">


----

### For Ivy, no cigar :(

<img src="img/ivy-build-errors.png">

---

### IDDQD

Ivy has some secret (private) APIs that give it superpowers!

----
    
    import { ɵcompileComponent } from '@angular/core';
    import { ɵrenderComponent as renderComponent, ɵmarkDirty } from '@angular/core';
    import '@angular/compiler';
    
    export class AppComponent {
      name = 'World';
    
      updateName(newName: string) {
        this.name = newName;
        ɵmarkDirty(this);
      }
    }
    
    ɵcompileComponent(AppComponent, {
      selector: 'a',
      template: `
        <h2>Hello, {{name}}</h2>
        <input (input)="updateName($event.target.value)" [value]="name">
      `
    });
    
    renderComponent(AppComponent);
       

----

### Can it compile without NgModule?

    ng build --prod
    
    
    
----

### Yes!!!

<img src="img/cheat-build.png">


----

### Does it run?

<img src="img/cheat-start.png">

### YES!!!


----

### Did we win?

<img src="img/cheat-result.png">


### Still no gaming with Angular

----

But we do get Higher-order components!

    import { Component, ɵrenderComponent, Injector } from '@angular/core';
    
    @HOC()
    @Component({
      selector: 'app-root',
      templateUrl: './app.component.html',
      styleUrls: ['./app.component.scss']
    })
    export class AppComponent {
      constructor(private injector: Injector) { }
      loadFeature() {
        import('./feature/feature/feature.component')
          .then(({ FeatureComponent }) => {
            ɵrenderComponent(FeatureComponent, { host: 'my-container', injector: this.injector });
          });
      }
    }
    
    export function HOC() {
      return (cmpType) => {
        const originalFactory = cmpType.ngComponentDef.factory;
        cmpType.ngComponentDef.factory = (...args) => {
          const cmp = originalFactory(...args);
          console.log(cmp);
          return cmp;
        };
      };
    }

---

### Lessons learned

- Angular 8 brings Ivy
- Bundle sizes are smaller, but not by much
- Ivy brings other benefits, such as better typing and better builds
- Not easy to upgrade a big existing project

---

### Other benefits from Angular 8

- dynamic imports in routes
- template typechecking
- no-module components
- higher-order-components   
- partial compiles
- polyfils!


---


### So when is Ivy the default?
 
Angular 8

<img src="img/opt-in.png">

----

### next.angular.io

Angular 9
 
<img src="img/opt-out.png">


---

The only question is is there more pizza left

---

Thank you

