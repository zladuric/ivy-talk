import { ɵcompileComponent } from '@angular/core';
import { ɵrenderComponent as renderComponent, ɵmarkDirty } from '@angular/core';
import '@angular/compiler';

export class AppComponent {
  name = 'World';

  updateName(newName: string) {
    this.name = newName;
    ɵmarkDirty(this);
  }
}

ɵcompileComponent(AppComponent, {
  selector: 'a',
  template: `
    <h2>Hello, {{name}}</h2>
    <input (input)="updateName($event.target.value)" [value]="name">
  `
});

renderComponent(AppComponent);
